import mysql.connector
import time

time.sleep(10) #Подключение к серверу
mydb = mysql.connector.connect(
  host="mysql-service",
  port=3306,
  user="root",
  password="root",
  database="lr_2_db"
)

mycursor = mydb.cursor()

n = 19 #Задание порогового значения

mycursor.execute("""
SELECT
  s.name AS student_name,
  s.date_of_birth
FROM
  students s
WHERE
  TIMESTAMPDIFF(YEAR, s.date_of_birth, CURDATE()) < %s;
""", (n,))

header = [row[0] for row in mycursor.description]

result = mycursor.fetchall()
print(result)

#Выполнили выборку, теперь положить локально в файл .csv
f = open('output/result2.csv', 'w')
head = ','.join(header)
f.write(str(head + '\n'))
for row in result:
    f.write(','.join(str(r) for r in row) + '\n')
f.close()

mydb.close()
